package main

import (
	"fmt"
	"html"
	"log"
	"math/rand"
	"net/http"
	"net/mail"
	"regexp"
	"time"

	"github.com/dgrijalva/jwt-go"

	"gitlab.com/krobolt/go-authserver/model"
	"gitlab.com/krobolt/go-authserver/prometheus"
)

type AuthServer struct {
	key *Key
}

//NewAuthServer New Authentication Sever.
func NewAuthServer(keys *Key) *AuthServer {
	return &AuthServer{
		key: keys,
	}
}

//RegisterDomain source of requests
//unsafe
func (as *AuthServer) RegisterDomain(w http.ResponseWriter, r *http.Request) {
	path := html.EscapeString(r.URL.Path)
	response := NewResponse()

	defer func() {
		if r := recover(); r != nil {
			log.Printf("Unable to satisfy request: %s. Recovery: %s \n", path, r)
		}
		Respond(response, w)
	}()

	if r.Method != http.MethodPut {
		response.Error = "method not allowed, only PUT supported"
		response.Code = http.StatusMethodNotAllowed
		return
	}
	//parse post data
	response.Code = http.StatusUnprocessableEntity
	r.ParseForm()
	domain, err := FormInput("domain", r)
	if err != nil {
		response.Error = "missing input domain"
		response.Code = http.StatusUnprocessableEntity
		return
	}

	key, err := FormInput("key", r)
	if err != nil {
		response.Error = "missing input key"
		response.Code = http.StatusUnprocessableEntity
		return
	}

	//ceeate domain/key entry in database
	domainmodel := model.NewDomainModel(model.NewModel(database, cache))
	ok, err := domainmodel.Insert("id", domain, key)
	if !ok {
		return
	}
	response.Code = http.StatusCreated
}

func (as *AuthServer) HndleDomainRequests(w http.ResponseWriter, r *http.Request) {
	path := html.EscapeString(r.URL.Path)
	response := NewResponse()

	defer func() {
		if r := recover(); r != nil {
			log.Printf("Unable to satisfy request: %s. Recovery: %s \n", path, r)
		}
		Respond(response, w)
	}()

	if r.Method != http.MethodPost {
		response.Error = "method not allowed, only POST supported"
		response.Code = http.StatusMethodNotAllowed
		return
	}
	domain, err := FormInput("domain", r)
	if err != nil {
		response.Error = "missing input domain"
		response.Code = http.StatusUnprocessableEntity
		return
	}
	token, err := FormInput("token", r)
	if err != nil {
		response.Error = "missing input token"
		response.Code = http.StatusUnprocessableEntity
		return
	}

	domainmodel := model.NewDomainModel(model.NewModel(database, cache))
	pubkey, err := domainmodel.GetKey(domain)
	if err != nil {
		return
	}

	type domainrequest struct {
		Action string
		jwt.StandardClaims
	}

	jt, err := VerifyRSAToken(token, pubkey)
	if err != nil {
		return
	}
	dreq := jt.Claims.(domainrequest)

	//check that the domain has authority
	//check token to enable resource server access

	fmt.Println(pubkey, dreq.Action)

}

func (as *AuthServer) RegAcc(username, email, domain string) (bool, *Response) {

	response := NewResponse()

	e, err := mail.ParseAddress(email)
	if err != nil {
		response.Error = "email invalid"
		return false, response
	}

	//autogenerate a password
	//not used as regenerated on activation
	password := GenKey(64)
	hash, err := HashPassword(password, 15)
	if err != nil {
		//unable to hash
		//internal error log
		log.Println(err)
		return false, response
	}

	usermodel := model.NewUserModel(model.NewModel(database, cache))
	usermodel.LeaveOpen()
	defer usermodel.Close()

	ok, _ := usermodel.QueryUsernameUnique(username)
	if !ok {
		response.Error = "username already taken"
		return false, response
	}
	ok, _ = usermodel.QueryEmailUnique(e.Address)
	if !ok {
		response.Error = "email already taken"
		return false, response
	}

	//create user ident
	userid := GenKey(32)
	servsecret := GenKey(16)
	usersecret := GenKey(16)
	dbsecret := servsecret + usersecret
	tokensecret, err := HashPassword(dbsecret, 15)
	if err != nil {
		log.Println(err)
		return false, response
	}

	token := NewActivationToken(userid, string(tokensecret), email)
	activationcode, err := GenerateActivationToken(token, as.key.RSA256.signKey)
	if err != nil {
		log.Println(err)
		return false, response
	}

	ok, err = usermodel.Insert(
		userid,
		username,
		e.Address,
		domain,
		string(hash),
		dbsecret,
	)
	if !ok {
		//unable to create record
		response.Error = "unknown error, please try again"
		log.Println(err)
		return false, response
	}
	//create activation cache object
	//expires same time as token
	ok, err = cache.Set(userid, dbsecret, time.Duration(time.Minute*15))
	if !ok {
		//unable to save cache
		response.Error = "unknown error, please try again"
		log.Println(err)
		//clean up and remove record from database
		return false, response
	}

	err = NewActivationEmail(e.Address, activationcode)
	if err != nil {
		response.Error = "unable to send activation email, please try later"
		log.Println(err)
		//clean up and remove record from database
		return false, response
	}
	err = NewRegisteredUserEmail(username)
	if err != nil {
		//internal message so log but continue
		log.Println(err)
	}

	response.Code = http.StatusCreated
	return true, response
}

func (as *AuthServer) RegisterAccount(w http.ResponseWriter, r *http.Request) {

	path := html.EscapeString(r.URL.Path)
	response := NewResponse()

	defer func() {
		if r := recover(); r != nil {
			log.Printf("Unable to satisfy request: %s. Recovery: %s \n", path, r)
		}
		Respond(response, w)
	}()

	if r.Method != http.MethodPut {
		response.Error = "method not allowed, only PUT supported"
		response.Code = http.StatusMethodNotAllowed
		return
	}

	//parse post data
	r.ParseForm()
	username, err := FormInput("username", r)
	if err != nil {
		response.Error = "missing input username"
		response.Code = http.StatusUnprocessableEntity
		return
	}
	email, err := FormInput("email", r)
	if err != nil {
		response.Error = "missing input email"
		return
	}

	ok, response := as.RegAcc(username, email, "domain")
	if !ok {
		return
	}
	response.Code = http.StatusCreated
	return
}

//ActivateAccount Process token and activate account if valid
func (as *AuthServer) ActivateAccount(w http.ResponseWriter, r *http.Request) {

	path := html.EscapeString(r.URL.Path)
	response := NewResponse()

	defer func() {
		if r := recover(); r != nil {
			log.Printf("Unable to satisfy request: %s. Recovery: %s \n", path, r)
		}
		Respond(response, w)
	}()

	if r.Method != http.MethodPut {
		response.Error = "method not allowed, only PUT supported"
		response.Code = http.StatusMethodNotAllowed
		return
	}

	r.ParseForm()
	code, err := FormInput("code", r)
	if err != nil {
		response.Error = "missing activiation code"
		response.Code = http.StatusUnprocessableEntity
		return
	}

	token, err := VerifyRSAToken(code, as.key.RSA256.verifyKey)
	if err != nil {
		response.Error = "invalid activiation code"
		response.Code = http.StatusUnauthorized
		return
	}

	atoken := token.Claims.(ActivationToken)
	cachedSecret, ok := cache.Get(atoken.Id)
	if !ok {
		response.Error = "missing activiation code"
		response.Code = http.StatusUnprocessableEntity
		return
	}
	cache.Client.Del(atoken.Id)
	if ok := VerifyPassword(cachedSecret, atoken.Secret); !ok {
		response.Error = "invalid activiation code"
		response.Code = http.StatusUnauthorized
		return
	}
	//token is valid
	//activate the email found in the token if the secret in the token matches
	usermodel := model.NewUserModel(model.NewModel(database, cache))
	usermodel.LeaveOpen()
	defer usermodel.Close()

	//update the userpassword
	//send email with activation information
	pwlen := rand.Intn(16)
	password := GenKey(16 + pwlen)
	hash, _ := HashPassword(password, 15)

	ok, _ = usermodel.ActivateUser(atoken.Id, hash)
	if !ok {
		//unable to activate
		return
	}

	ConfimActivationEmail(atoken.Email, password)
	response.Code = http.StatusAccepted
}

func (as *AuthServer) HandleLogin(w http.ResponseWriter, r *http.Request) {

	path := html.EscapeString(r.URL.Path)
	response := NewResponse()

	defer func() {
		if r := recover(); r != nil {
			log.Printf("Unable to satisfy request: %s. Recovery: %s \n", path, r)
			prometheus.CriticalError.Observe(float64(response.Code))
		}
		Respond(response, w)
	}()

	switch path {
	case "/login":
		as.Login(r, response)
		break
	default:
		response.Code = http.StatusNotFound
		response.Error = "page not found"
	}
}

//Login to authserver
//Login to gain access/authorization which can be freshed to avoid logging in again
func (as *AuthServer) Login(r *http.Request, resp *Response) {

	//get connection ip, origin and other ident information
	c := NewConnection(r)

	//login does not have to be authenticated.
	//grab hash from database and attempt login against provided password
	//generate new token
	authed := &UserData{}

	//generate auth code from password
	authcode := GenKey(64)
	authRequest := NewAuthRequestToken(c.id, c.origin, authcode, *authed)
	token, err := GenerateRSAToken(authRequest, as.key.RSA256.signKey)
	if err != nil {
		resp.Code = http.StatusUnauthorized
		resp.Error = "login failed"
		return
	}

	/*
		<a href="https://authorization-server.com/oauth/authorize
		?response_type=code&client_id=mRkZGFjM&state=5ca75bd30">
		Connect Your Account</a>

			https://example-app.com/cb?code=Yzk5ZDczMzRlNDEwY&state=5ca75bd30

			POST /oauth/token HTTP/1.1
			Host: authorization-server.com

			code=Yzk5ZDczMzRlNDEwY
			&grant_type=code
			&redirect_uri=https://example-app.com/cb
			&client_id=mRkZGFjM
			&client_secret=ZGVmMjMz

			{
				"access_token": "AYjcyMzY3ZDhiNmJkNTY",
				"refresh_token": "RjY2NjM5NzA2OWJjuE7c",
				"token_type": "bearer",
				"expires": 3600
			}
	*/

	resp.Code = 200
	resp.Token = token
	return
}

func (as *AuthServer) Handle(w http.ResponseWriter, r *http.Request) {
	//c := NewConnection(r)
	path := html.EscapeString(r.URL.Path)
	response := NewResponse()

	//handle response at end
	//catch any panics
	defer func() {
		if r := recover(); r != nil {
			log.Printf("Unable to satisfy request: %s. Recovery: %s \n", path, r)
			prometheus.CriticalError.Observe(float64(response.Code))
		}
		Respond(response, w)
	}()

	if r.Method == http.MethodGet {
		response.Error = "method not allowed, GET not supported"
		response.Code = http.StatusMethodNotAllowed
		return
	}

	authorizationToken := r.Header.Get("Authorization")
	authorizationURL := r.Header.Get("AuthorizationURL")

	//check for cheap early returns
	response.Code = http.StatusUnauthorized
	if authorizationToken == "" {
		response.Error = "unable to locate Authorization token"
		return
	}

	token, err := VerifyWithClaimsRSAToken(authorizationToken, as.key.RSA256.verifyKey)
	claims := token.Claims.(*ResponseToken)
	fmt.Println(claims)
	if err != nil {
		//long expire token shouldnt of expired. will need to log in again
	}

	//check that auth header is
	//auth header has been confirmed to be valid and not tampered with

	switch authorizationURL {
	case "authRequest":
		//upgrade auth request to auth grant if they have
		//permission to do the requested grant
		//get grant from body
		//check grant against user access
		//upgrade if conditions met
		authorizationURL = "authGrant"
	case "authGrant":
		//upgrade to access code
		//check that
		authorizationURL = "accessCode"
	case "access":
		//check access token
		//if invalid check refresh token
		//gen new access token
		//return to callback
		break
	default:
		response.Code = http.StatusNotFound
		response.Error = "url not found"
	}

}

func _getpassword(r *http.Request, response *Response) {

	password, err := FormInput("password", r)
	if err != nil {
		response.Error = "missing input password"
		return
	}

	if len(password) >= 8 { //out
		response.Error = "password must be at least 8 characters long"
	}
	if len(password) < 64 {
		response.Error = "password can not be more than 64 characters long"
	}
	m := regexp.MustCompile(`[^A-Za-z0-9]`)
	re := m.FindStringSubmatch(password)
	if len(re) == 0 {
		response.Error = "password must have at least 1 special character"
	}
	m = regexp.MustCompile(`(.*[A-Z].*)`)
	re = m.FindStringSubmatch(password)
	if len(re) == 0 {
		response.Error = "password must have at least one uppercase letter"
	}
	m = regexp.MustCompile(`(.*[a-z].*)`)
	re = m.FindStringSubmatch(password)
	if len(re) == 0 {
		response.Error = "password must have at least one lowercase letter"
	}
	m = regexp.MustCompile(`(.*\d.*)`)
	re = m.FindStringSubmatch(password)
	if len(re) == 0 {
		response.Error = "password must have at least one digit"
	}
}
