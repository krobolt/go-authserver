package main

import (
	"testing"

	"golang.org/x/crypto/bcrypt"
)

func Test_HashPassword(t *testing.T) {
	pw := "password"
	expected := "$2a$10$io0rAcdt4YTCCAKIOrFT9OQN9L7OFg.SMoTPOGpksA2uoncMJ1GGG"
	err := bcrypt.CompareHashAndPassword([]byte(expected), []byte(pw))
	if err != nil {
		t.Error("password failed to hash")
	}
}

func Test_VerifyPassword(t *testing.T) {
	pw := "password"
	hash := "$2a$10$EEpu/Z4DheUFmQtg8gM7X.RsqXXDhgsetINzVS5V7Yy4.kn4ZfXey"
	ok := VerifyPassword(pw, hash)
	if !ok {
		t.Error("password failed to verify password hash")
	}
}
