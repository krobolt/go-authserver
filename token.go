package main

import (
	"crypto/rsa"
	"log"
	"os"
	"time"

	"github.com/dgrijalva/jwt-go"
)

//authRequest [refresh token]
//basic token that contains basic account information
//long expiry, 1 week.
//multiple use assuming not blacklisted and client/ip origin validation passes
//need to keep track fo this
//expensive lookup to database
type authRequest struct {
	ClientOrigin string
	AuthCode     string
	UserData
	jwt.StandardClaims
}

//authGrant
//expires on use, remove from whitelist
//sho                              mins
type authGrant struct {
	id          string
	accessToken string //access request token
}

//access token used to access protected resource
//single use, remove from whitelist
//very short. 5 mins
type accessToken struct {
	resource string
}

type ResponseToken struct {
	Domain string `json:"domain"`
	authRequest
	jwt.StandardClaims
}

type ActivationToken struct {
	Secret string
	Email  string
	jwt.StandardClaims
}

func NewActivationToken(id, secret, email string) ActivationToken {
	return ActivationToken{
		secret,
		email,
		jwt.StandardClaims{
			Audience:  "auth.server.address",
			ExpiresAt: time.Now().Add(time.Minute * 15).Unix(),
			Id:        id,
			IssuedAt:  time.Now().Unix(),
			Issuer:    os.Getenv("TOKEN_ISSUER"),
			NotBefore: time.Now().Unix(),
			Subject:   "account activation",
		},
	}
}

func GenerateActivationToken(token ActivationToken, key *rsa.PrivateKey) (string, error) {
	jwtout := jwt.NewWithClaims(jwt.GetSigningMethod("RS256"), token)
	return jwtout.SignedString(key)
}

func GenerateRSAToken(token authRequest, key *rsa.PrivateKey) (string, error) {
	jwtout := jwt.NewWithClaims(jwt.GetSigningMethod("RS256"), token)
	return jwtout.SignedString(key)
}

func VerifyRSAToken(tokenString string, verifyKey *rsa.PublicKey) (*jwt.Token, error) {
	return jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		// since we only use the one private key to sign the tokens,
		// we also only use its public counter part to verify
		return verifyKey, nil
	})
}

func VerifyWithClaimsRSAToken(tokenString string, verifyKey *rsa.PublicKey) (*jwt.Token, error) {
	return jwt.ParseWithClaims(tokenString, &ResponseToken{}, func(token *jwt.Token) (interface{}, error) {
		// since we only use the one private key to sign the tokens,
		// we also only use its public counter part to verify
		return verifyKey, nil
	})
}

func NewAuthRequestToken(id, origin, authcode string, user UserData) authRequest {
	return authRequest{
		origin,
		authcode,
		user,
		jwt.StandardClaims{
			Audience:  "auth.server.address",
			ExpiresAt: time.Now().Add(time.Hour * 48).Unix(),
			Id:        id,
			IssuedAt:  time.Now().Unix(),
			Issuer:    os.Getenv("TOKEN_ISSUER"),
			NotBefore: time.Now().Unix(),
			Subject:   "authentication",
		},
	}
}

func NewResponseToken(id, origin, authcode string, user UserData) *ResponseToken {
	return &ResponseToken{
		os.Getenv("TOKEN_DOMAIN"),
		NewAuthRequestToken(id, origin, authcode, user),
		jwt.StandardClaims{
			Audience:  "auth.server.address",
			ExpiresAt: time.Now().Add(time.Minute * 20).Unix(),
			Id:        id,
			IssuedAt:  time.Now().Unix(),
			Issuer:    os.Getenv("TOKEN_ISSUER"),
			NotBefore: time.Now().Unix(),
			Subject:   "response subject",
		},
	}
}

func VerifyToken(tokenString string, secret string) (bool, *ResponseToken) {
	token, err := jwt.ParseWithClaims(tokenString, &ResponseToken{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(secret), nil
	})
	if err != nil {
		log.Println(err)
	}
	if claims, ok := token.Claims.(*ResponseToken); ok && token.Valid {
		return true, claims
	}
	return false, nil
}
