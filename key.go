package main

import (
	"crypto/md5"
	"crypto/rsa"
	"encoding/hex"
	"io/ioutil"

	"github.com/dgrijalva/jwt-go"
)

type Key struct {
	HS256  string
	RSA256 *rsa256
}

type rsa256 struct {
	verifyKey   *rsa.PublicKey
	verifyBytes []byte
	signKey     *rsa.PrivateKey
	signBytes   []byte
}

func NewServerKeys(secret, pubFilename, privFilename string) (*Key, error) {
	var (
		signKey   *rsa.PrivateKey
		verifyKey *rsa.PublicKey
		k         = &Key{
			HS256:  secret,
			RSA256: &rsa256{},
		}
	)
	signBytes, err := ioutil.ReadFile(privFilename)
	if err != nil {
		return k, err
	}
	k.RSA256.signBytes = signBytes

	signKey, err = jwt.ParseRSAPrivateKeyFromPEM(signBytes)
	if err != nil {
		return k, err
	}
	k.RSA256.signKey = signKey
	verifyBytes, err := ioutil.ReadFile(pubFilename)
	if err != nil {
		return k, err
	}
	k.RSA256.verifyBytes = verifyBytes
	verifyKey, err = jwt.ParseRSAPublicKeyFromPEM(verifyBytes)
	if err != nil {
		return k, err
	}
	k.RSA256.verifyKey = verifyKey
	return k, nil
}

func MD5(s string) string {
	hash := md5.Sum([]byte(s))
	id := hex.EncodeToString(hash[:])
	return id
}
