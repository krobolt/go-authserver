package main

import (
	"crypto/tls"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"gitlab.com/krobolt/go-authserver/prometheus"
)

func NewHttpServer(mux *http.ServeMux) *http.Server {
	return &http.Server{
		Addr:         HttpAddr,
		Handler:      mux,
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
	}
}

func SSLredirect(w http.ResponseWriter, req *http.Request) {
	target := "https://" + req.Host + req.URL.Path
	if len(req.URL.RawQuery) > 0 {
		target += "?" + req.URL.RawQuery
	}
	http.Redirect(w, req, target, http.StatusTemporaryRedirect)
}

func RateLimit(h http.Handler, n int) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		c := NewConnection(r)
		hit, ok := cache.Get(c.id)
		if ok {
			i, err := strconv.Atoi(hit)
			if err == nil {
				i++
				c.hits = i
			}
		}
		if c.hits > n {
			response := NewResponse()
			response.Code = 405
			response.Error = "too many attempts"
			Respond(response, w)
			return
		}

		h.ServeHTTP(w, r)
		cache.Set(c.id, c.hits, time.Minute)
	})
}

func Prometheus(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("running Prometheus middleware setting fixed value: 10")
		next.ServeHTTP(w, r)
		prometheus.LoginRequestsResponseTime.Observe(float64(10))
	})
}

func MaxConnections(h http.Handler, n int) http.Handler {
	block := make(chan int, n)
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		block <- 1
		defer func() { <-block }()
		h.ServeHTTP(w, r)
	})
}

func NewSecureServer(mux *http.ServeMux) http.Server {
	cfg := &tls.Config{
		MinVersion:               tls.VersionTLS12,
		CurvePreferences:         []tls.CurveID{tls.CurveP521, tls.CurveP384, tls.CurveP256},
		PreferServerCipherSuites: true,
		CipherSuites: []uint16{
			tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA,
			tls.TLS_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_RSA_WITH_AES_256_CBC_SHA,
		},
	}
	return http.Server{
		Addr:         ServerAddr,
		Handler:      MaxConnections(Prometheus(mux), 1),
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
		TLSConfig:    cfg,
		TLSNextProto: make(map[string]func(*http.Server, *tls.Conn, http.Handler), 0),
	}
}
