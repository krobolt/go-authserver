package main

import (
	"fmt"
	"net/http"
	"net/url"

	"gitlab.com/krobolt/go-authserver/model"
)

func UrlParam(name string, v url.Values) (string, error) {
	if len(v[name][0]) >= 1 {
		return v[name][0], nil
	}
	return "", fmt.Errorf("unable to locate url param key: %s", name)
}

func FormInput(name string, r *http.Request) (string, error) {
	input := r.Form.Get(name)
	if input == "" {
		return input, fmt.Errorf("missing input: %s", name)
	}
	return input, nil
}

func (as *AuthServer) GetLoginForm(r *http.Request) (string, string, error) {
	r.ParseForm()
	username, err := FormInput("Username", r)
	password, err := FormInput("Username", r)
	if err != nil {
		return "", "", err
	}
	return username, password, err
}

func (as *AuthServer) GetNewUserParams(r *http.Request) (*model.PrivateUser, error) {

	vars := r.URL.Query()
	firstname, err := UrlParam("firstname", vars)
	lastname, err := UrlParam("lastname", vars)
	username, err := UrlParam("username", vars)
	email, err := UrlParam("email", vars)
	domain, err := UrlParam("domain", vars)

	return model.NewPrivateUserInfo(
		username,
		email,
		domain,
		firstname,
		lastname,
	), err

}
