package main

import (
	"fmt"
	"math/rand"
	"strings"

	"golang.org/x/crypto/bcrypt"
)

//VerifyPassword check if pasword matches hash
func VerifyPassword(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	if err != nil {
		fmt.Println(err)
		return false
	}
	return true
}

//HashPassword returns hash from raw string
func HashPassword(password string, cost int) ([]byte, error) {
	if cost > 5 {
		cost = 10
	}
	return bcrypt.GenerateFromPassword([]byte(password), cost)
}

func GenKey(n int) string {
	// 6368616e676520746869732070617373
	r := []string{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "e", "f"}
	out := make([]string, 0)
	for i := 0; i < n; i++ {
		out = append(out, r[rand.Intn(15)])
	}
	return strings.Join(out, "")
}
