package main

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"time"

	"github.com/go-redis/redis"
	"github.com/go-sql-driver/mysql"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/krobolt/go-authserver/model"
	"gitlab.com/krobolt/go-authserver/storage"
)

const (
	ServerAddr = "0.0.0.0:443"
	HttpAddr   = "0.0.0.0:80"
)

var (
	ErrMissingServerKeys = errors.New("server keys not found")
	ErrDatabaseOffline   = errors.New("unable to connect to database")
	ErrCacheOffline      = errors.New("unable to connect to redis")
	database             *storage.DBC
	cache                *storage.RedisCache
)

func main() {
	keys, err := NewServerKeys(
		os.Getenv("TOKEN_HS256_SECRET"),
		os.Getenv("TOKEN_RSA_PUB"),
		os.Getenv("TOKEN_RSA_PRIV"),
	)
	//not much point in doing much else without provided keys
	if err != nil {
		panic(ErrMissingServerKeys)
	}

	mysqlconfig := &mysql.Config{
		User:                 os.Getenv("DB_USER"),
		Passwd:               os.Getenv("DB_PASSWORD"),
		DBName:               os.Getenv("DB_TABLE"),
		Net:                  "tcp",
		Addr:                 os.Getenv("DB_HOST"),
		AllowNativePasswords: true,
		//Params           map[string]string // Connection parameters
		//Collation        string            // Connection collation
		Timeout:      time.Minute * 15,
		ReadTimeout:  time.Minute * 3,
		WriteTimeout: time.Minute * 3, // I/O write timeout
	}

	database = storage.NewDatabase(mysqlconfig)
	redisdatabase, err := strconv.Atoi(os.Getenv("REDIS_DB"))
	if err != nil {
		redisdatabase = 0
	}

	cache = &storage.RedisCache{
		Client: redis.NewClient(&redis.Options{
			Addr:     os.Getenv("REDIS_ADDR"),
			Password: os.Getenv("REDIS_PASS"),
			DB:       redisdatabase,
		}),
	}

	if !cache.Test() {
		panic(ErrCacheOffline)
	}

	testDb, err := database.Open()
	defer testDb.Close()
	if err != nil {
		panic(err)
	} else {
		//create user tables
		usermodel := model.NewUserModel(model.NewModel(database, cache))
		usermodel.Up()
	}

	AuthServ := NewAuthServer(keys)
	fmt.Println(AuthServ)
	mux := http.NewServeMux()

	//app level:
	//forms/pages
	//register account
	//acivate account
	//login form -> login -> fist sign in update password
	//profile/edit screen
	//logout

	mux.HandleFunc("/login", AuthServ.HandleLogin)
	mux.HandleFunc("/account/register", AuthServ.RegisterAccount)
	mux.HandleFunc("/account/activate", AuthServ.ActivateAccount)
	mux.HandleFunc("/", AuthServ.Handle)

	//mux.HandleFunc("/debug/pprof/", pprof.Index)
	//mux.HandleFunc("/debug/pprof/cmdline", pprof.Cmdline)
	//mux.HandleFunc("/debug/pprof/profile", pprof.Profile)
	//mux.HandleFunc("/debug/pprof/symbol", pprof.Symbol)
	//mux.HandleFunc("/debug/pprof/trace", pprof.Trace)

	httpmux := http.NewServeMux()
	httpmux.Handle("/", promhttp.Handler())
	//mux.Handle("/", promhttp.Handler())

	httpServer := NewHttpServer(httpmux)
	secureServer := NewSecureServer(mux)

	go func() {
		if err := httpServer.ListenAndServe(); err != nil {
			log.Println(err)
		}
	}()

	go func() {
		log.Fatal(secureServer.ListenAndServeTLS("key/cert.pem", "key/key.pem"))
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*15)
	defer cancel()
	secureServer.Shutdown(ctx)
	httpServer.Shutdown(ctx)

	log.Println("shutting down")
	os.Exit(0)
}
