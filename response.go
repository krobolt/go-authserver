package main

import (
	"encoding/json"
	"log"
	"net/http"
)

//Request from client
type Request struct {
	Token string
}

//Response to be sent
type Response struct {
	Code  int
	Error string
	Token string
}

//LoginData provided inputs for login
type LoginData struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

//UserData publically available information for user
type UserData struct {
	Username string `json:"username"`
	Access   int
}

//Private data for user
type UserDetails struct {
	UserData
	Email string `json:"email"`
	Hash  string
}

func NewResponse() *Response {
	return &Response{
		Code:  http.StatusInternalServerError,
		Error: "Internal Server Error",
		Token: "",
	}
}

func (r *Response) WriteJson(w http.ResponseWriter) {
	js, err := json.Marshal(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

func JsonDecodeBody(r *http.Request, v interface{}) error {
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&v)
	if err != nil {
		log.Println(err)
	}
	return err
}

func Respond(r *Response, w http.ResponseWriter) {
	w.Header().Add("Strict-Transport-Security", "max-age=63072000; includeSubDomains")
	w.WriteHeader(r.Code)
	r.WriteJson(w)
}

//UpgradeUserResponseToken adds UserDetails to Default response
func UpgradeUserResponseToken(ud *UserDetails, res *ResponseToken) *ResponseToken {
	res.Username = ud.Username
	res.Access = ud.Access
	return res
}
