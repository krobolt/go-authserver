package storage

import (
	"fmt"
	"time"

	"github.com/go-redis/redis"
)

type RedisCache struct {
	Client *redis.Client
}

//GetClient returns redis client.
func (r *RedisCache) GetClient() *redis.Client {
	return r.Client
}

//Test connection
func (r *RedisCache) Test() bool {
	p, _ := r.Client.Ping().Result()
	if p == "PONG" {
		return true
	}
	return false
}

//Set string value in keystore
func (r *RedisCache) Set(key string, data interface{}, expires time.Duration) (bool, error) {
	err := r.Client.Set(key, data, expires).Err()
	if err != nil {
		fmt.Println("Unable to store value")
		return false, err
	}
	return true, err
}

//Get string value from keystore
func (r *RedisCache) Get(key string) (string, bool) {
	v, err := r.Client.Get(key).Result()
	if err != nil {
		return "", false
	}
	return v, true
}

//Scan returns collection of keys for pattern
func (r *RedisCache) Scan(pattern string) ([]string, error) {
	var cursor uint64
	var keys []string
	var err error
	for {
		keys, cursor, err = r.Client.Scan(cursor, pattern, 10).Result()
		if err != nil {
			return keys, err
		}
		if cursor == 0 {
			break
		}
	}
	return keys, err
}
