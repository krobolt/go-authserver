package storage

import (
	"database/sql"

	"github.com/go-sql-driver/mysql"
)

type DBC struct {
	Config *mysql.Config
}

//NewDatabase returns database config.
func NewDatabase(config *mysql.Config) *DBC {
	return &DBC{
		Config: config,
	}
}

//exampleConfig
var exampleConfig = &mysql.Config{
	User:   "user",
	Passwd: "pass",
	DBName: "db",
	Net:    "tcp",
	//Addr:   "localhost:3306",
	//Params           map[string]string // Connection parameters
	//Collation        string            // Connection collation
	//Timeout      time.Duration // Dial timeout
	//ReadTimeout  time.Duration // I/O read timeout
	//WriteTimeout time.Duration // I/O write timeout
}

//Open opens a database connection
//test database connection on open.]
func (dbc *DBC) Open() (*sql.DB, error) {
	db, err := sql.Open("mysql", dbc.Config.FormatDSN())
	if err != nil {
		return nil, err
	}

	err = db.Ping()
	if err != nil {
		return db, err
	}
	return db, nil
}

//OpenDB opens a database connection
//test database connection on open.]
func OpenDB(dsn string) (*sql.DB, error) {
	db, err := sql.Open("mysql", dsn)
	if err != nil {
		return nil, err
	}
	err = db.Ping()
	if err != nil {
		return db, err
	}
	return db, nil
}
