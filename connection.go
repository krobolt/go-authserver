package main

import (
	"crypto/md5"
	"encoding/hex"
	"net"
	"net/http"
)

type connection struct {
	id            string
	origin        string
	hits          int
	loginattempts int
}

func NewConnection(r *http.Request) *connection {
	ua := r.Header.Get("User-Agent")
	clientip := GetClientIp(r.RemoteAddr, r.Header.Get("X-Forwarded-For"))
	clientLocation := clientip
	clienthash := md5.Sum([]byte(clientLocation))
	originhash := md5.Sum([]byte(ua + clientLocation))

	return &connection{
		id:     hex.EncodeToString(clienthash[:]),
		origin: hex.EncodeToString(originhash[:]),
		hits:   0,
	}
}

func GetClientIp(RemoteAddr, XForward string) string {
	ip, port, err := net.SplitHostPort(RemoteAddr)
	userIP := net.ParseIP(ip)
	if err != nil {
		ip = XForward
	}
	if userIP != nil {
		ip = userIP.String()
	}
	return ip + port + XForward

}
