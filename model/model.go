package model

import (
	"database/sql"

	"gitlab.com/krobolt/go-authserver/storage"
)

type Model struct {
	db    *storage.DBC
	cache *storage.RedisCache
	open  bool
	conn  *sql.DB
}

func NewModel(db *storage.DBC, cache *storage.RedisCache) *Model {
	return &Model{
		db:    db,
		cache: cache,
	}
}

func (m *Model) LeaveOpen() error {
	m.open = true
	db, err := m.db.Open()
	if err != nil {
		return err
	}
	m.conn = db
	return nil
}
func (m *Model) Close() {
	if m.open {
		m.conn.Close()
	}
}
