package model

import (
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"errors"
	"strings"
	"time"
)

type UserModel struct {
	*Model
}

//User data publically available information for user
type User struct {
	Username string `json:"username"`
	Email    string `json:"email"`
	Domain   string `json:"domain"`
}

//PrivateUser private data for user
type PrivateUser struct {
	*User
	ID        string `json:"-"` //hide from accidental marshal
	FirstName string `json:"-"`
	LastName  string `json:"-"`
	Hash      string `json:"-"`
	Secret    string `json:"-"`
	IsActive  bool   `json:"-"`
}

const (
	sqlUsersUp = `
	CREATE TABLE USER_TABLE(
	Userid varchar(50) NOT NULL UNIQUE,
	Last_Name varchar(50) NOT NULL,
	First_Name varchar(50) NOT NULL,
	Username varchar(50) NOT NULL UNIQUE,	
	Email varchar(90) NOT NULL UNIQUE,	
	Domain varchar(120) NOT NULL,	
	Hash varchar(150) NOT NULL,	
	Secret varchar(150) NOT NULL,	
	Is_Active bool,
	PRIMARY KEY (Userid)
	); 
`

	sqlUserInsert           = `INSERT Username=?,Email=1,Domain=?,Hash=?,Secret=?,Is_Active=?, Activated=? INTO Users`
	sqlUserVerifyActivation = `SELECT Secret from USER_TABLE WHERE Userid = ?`
	sqlUserActivate         = `UPDATE USER_TABLE SET Is_Active = 1 hash = ? WHERE Userid = ?;`
	sqlLoginUsername        = `SELECT Username, Email, Hash from USER_TABLE WHERE Username = ?`
	sqlUserDetails          = `SELECT Userid, Last_Name,First_Name,Domain from USER_TABLE WHERE Userid = ?`
	sqlUsernameUnique       = `SELECT Username from USER_TABLE WHERE Username = ? LIMIT 1`
	sqlEmailUnique          = `SELECT Email from USER_TABLE WHERE Email = ? LIMIT 1`
	sqlUserSecret           = `SELECT Secret from USER_TABLE WHERE Userid = ? LIMIT 1`
	sqlUserUpdateSecret     = `"UPDATE Secret=? WHERE Userid=?"`
	sqlUserUpdatePassword   = `"UPDATE Hash=? WHERE Userid=?"`
	sqlUserDelete           = `"UPDATE Is_Active=? WHERE Userid=?"`
)

func NewUserModel(m *Model) *UserModel {
	return &UserModel{m}
}

func NewPrivateUserInfo(username, email, domain, firstname, secondname string) *PrivateUser {
	return &PrivateUser{
		&User{
			username,
			email,
			domain,
		},
		"defaultid",
		firstname,
		secondname,
		"defaulthash",
		"defaultsecret",
		false,
	}
}

func NewPrivateUser() *PrivateUser {
	return &PrivateUser{
		&User{
			"default_username",
			"default_email",
			"default_domain",
		},
		"defaultid",
		"default_firstname",
		"default_secondname",
		"defaulthash",
		"defaultsecret",
		false,
	}
}

//Up execute create user model table
func (user *UserModel) Up() (bool, error) {
	db, err := user.db.Open()
	defer db.Close()
	if err != nil {
		return false, err
	}
	stmt, err := db.Prepare(sqlUsersUp)
	defer stmt.Close()
	res, err := stmt.Exec()
	if err != nil {
		return false, err
	}
	_, err = res.LastInsertId()
	if err != nil {
		return false, err
	}
	return true, err
}

//Insert new user  `INSERT Userid=?, Username=?,Email=1,Domain=?,Hash=?,Secret=?,Is_Active=?, Activated=? INTO Users`
func (user *UserModel) Insert(
	id,
	username,
	email,
	domain,
	hash,
	secretActivation string,
) (bool, error) {

	db := user.conn
	if !user.open {
		dbc, err := user.db.Open()
		if err != nil {
			return false, err
		}
		db = dbc
		defer db.Close()
	}

	stmt, err := db.Prepare(sqlUserInsert)
	defer stmt.Close()
	res, err := stmt.Exec(
		id,
		username,
		email,
		domain,
		hash,
		secretActivation,
		false,
		false,
	)
	if err != nil {
		return false, err
	}
	_, err = res.LastInsertId()
	if err != nil {
		return false, err
	}
	return true, err
}

//ActivateUser `UPDATE USER_TABLE SET Is_Active = 1 WHERE Userid = ?;`
func (user *UserModel) ActivateUser(id string, hash []byte) (bool, error) {
	db := user.conn
	if !user.open {
		dbc, err := user.db.Open()
		if err != nil {
			return false, err
		}
		db = dbc
		defer db.Close()
	}
	stmt, err := db.Prepare(sqlUserActivate)
	defer stmt.Close()
	_, err = stmt.Exec(string(hash), id)
	if err != nil {
		return false, err
	}
	return true, nil
}

//GetActivation (atoken.Username, atoken.Secret)
func (user *UserModel) GetActivation(id string) (string, error) {
	var dbsecret string
	db := user.conn
	if !user.open {
		dbc, err := user.db.Open()
		if err != nil {
			return dbsecret, err
		}
		db = dbc
		defer db.Close()
	}
	stmt, err := db.Prepare(sqlUserVerifyActivation)
	defer stmt.Close()
	rows, err := stmt.Query(id)
	defer rows.Close()
	if err != nil {
		return dbsecret, err
	}
	for rows.Next() {
		err = rows.Scan(&dbsecret)
		if err != nil {
			return dbsecret, err
		}
	}

	return dbsecret, err
}

//FetchUser returns sensistive user infomration from database
//not saved to cache
func (user *UserModel) FetchUser(username string) (*PrivateUser, error) {
	//no need to cache result
	//contains sensistive information best not save to redis
	return user.queryUser(username)
}

//FetchLogin retrives login information from database
//checks cache storage first
//saves to cache if result found
func (user *UserModel) FetchLogin(username string) (*PrivateUser, error) {
	hash := md5.Sum([]byte(sqlLoginUsername + username))
	cacheid := hex.EncodeToString(hash[:])
	dbuser := NewPrivateUser()
	ok := user.fetchCache(cacheid, dbuser)
	if ok {
		return dbuser, nil
	}
	dbuser, err := user.queryLogin(username)
	ok = user.saveCache(cacheid, dbuser)
	if !ok {
		return dbuser, errors.New("unable to cache login information")
	}
	return dbuser, err
}

//QueryUsernameUnique check if username already in database
func (user *UserModel) QueryUsernameUnique(username string) (bool, error) {
	return user.queryUnique(username, sqlUsernameUnique)
}

//QueryEmailUnique check if email already in database
func (user *UserModel) QueryEmailUnique(email string) (bool, error) {
	return user.queryUnique(email, sqlEmailUnique)
}

func (user *UserModel) queryUnique(input, sql string) (bool, error) {
	db := user.conn
	if !user.open {
		dbc, err := user.db.Open()
		if err != nil {
			return false, err
		}
		db = dbc
		defer db.Close()
	}
	stmt, err := db.Prepare(sql)
	defer stmt.Close()
	rows, err := stmt.Query(input)
	defer rows.Close()
	if err != nil {
		return false, err
	}
	result := ""
	for rows.Next() {
		err = rows.Scan(&result)
		if err != nil {
			return false, err
		}
	}
	if result != "" {
		return false, nil
	}
	return true, nil
}

func (user *UserModel) queryUser(username string) (*PrivateUser, error) {
	dbuser := NewPrivateUser()
	db := user.conn
	if !user.open {
		dbc, err := user.db.Open()
		if err != nil {
			return dbuser, err
		}
		db = dbc
		defer db.Close()
	}
	stmt, err := db.Prepare(sqlLoginUsername)
	defer stmt.Close()
	rows, err := stmt.Query(username)
	defer rows.Close()
	if err != nil {
		return dbuser, err
	}
	for rows.Next() {
		err = rows.Scan(&dbuser.Username, &dbuser.Email, &dbuser.Hash)
		if err != nil {
			return dbuser, err
		}
	}
	return dbuser, nil
}

func (user *UserModel) queryLogin(username string) (*PrivateUser, error) {
	dbuser := NewPrivateUser()
	db := user.conn
	if !user.open {
		dbc, err := user.db.Open()
		if err != nil {
			return dbuser, err
		}
		db = dbc
		defer db.Close()
	}
	stmt, err := db.Prepare(sqlLoginUsername)
	defer stmt.Close()
	rows, err := stmt.Query(username)
	defer rows.Close()
	if err != nil {
		return dbuser, err
	}
	for rows.Next() {
		err = rows.Scan(&dbuser.Username, &dbuser.Email, &dbuser.Hash)
		if err != nil {
			return dbuser, err
		}
	}
	return dbuser, nil
}

func (user *UserModel) fetchCache(id string, dst *PrivateUser) bool {
	result, ok := user.cache.Get(id)
	if ok {
		decoder := json.NewDecoder(strings.NewReader(result))
		err := decoder.Decode(dst)
		if err != nil {
			return false
		}
		return true
	}
	return false
}

func (user *UserModel) saveCache(id string, source *PrivateUser) bool {
	b, err := json.Marshal(source)
	if err != nil {
		return false
	}
	ok, err := user.cache.Set(id, string(b), time.Minute*3)
	if !ok || err != nil {
		return false
	}
	return true
}
