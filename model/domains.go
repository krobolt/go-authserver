package model

import (
	"crypto/rsa"

	"github.com/dgrijalva/jwt-go"
)

type DomainModel struct {
	*Model
}

const (
	sqlDomainUp = `
	CREATE TABLE DOMAIN_TABLE(
	DomainId varchar(50) NOT NULL UNIQUE,
	Domain varchar(120) NOT NULL,	
	Key varchar(150) NOT NULL,	
	Is_Active bool,
	PRIMARY KEY (DomainId)
	); 
`
	sqDomainInsert    = `INSERT DomainId=?,Domain=?,Key=?,Is_Active=1 INTO DOMAIN_TABLE`
	sqDomainSelectKey = `INSERT DomainId=?,Domain=?,Key=?,Is_Active=1 INTO DOMAIN_TABLE`
)

func NewDomainModel(m *Model) *DomainModel {
	return &DomainModel{m}
}

func (d *DomainModel) Up() (bool, error) {
	db, err := d.db.Open()
	defer db.Close()
	if err != nil {
		return false, err
	}
	stmt, err := db.Prepare(sqlDomainUp)
	defer stmt.Close()
	res, err := stmt.Exec()
	if err != nil {
		return false, err
	}
	_, err = res.LastInsertId()
	if err != nil {
		return false, err
	}
	return true, err
}

func (d *DomainModel) GetKey(domain string) (*rsa.PublicKey, error) {
	//extract from database
	var verifyKey *rsa.PublicKey
	var verifyBytes []byte
	var keystring string

	db := d.conn
	if !d.open {
		dbc, err := d.db.Open()
		if err != nil {
			return verifyKey, err
		}
		db = dbc
		defer db.Close()
	}

	stmt, err := db.Prepare(sqDomainSelectKey)
	defer stmt.Close()
	rows, err := stmt.Query(domain)
	defer rows.Close()
	if err != nil {
		return verifyKey, err
	}
	for rows.Next() {
		err = rows.Scan(&keystring)
		if err != nil {
			return verifyKey, err
		}
	}

	verifyBytes = []byte(keystring)
	if err != nil {
		return verifyKey, err
	}
	verifyKey, err = jwt.ParseRSAPublicKeyFromPEM(verifyBytes)
	return verifyKey, err
}

//Insert  `INSERT DomainId=?,Domain=?,Key=?,Is_Active=1 INTO DOMAIN_TABLE`
func (d *DomainModel) Insert(
	id,
	domain,
	key string,
) (bool, error) {

	db := d.conn
	if !d.open {
		dbc, err := d.db.Open()
		if err != nil {
			return false, err
		}
		db = dbc
		defer db.Close()
	}

	stmt, err := db.Prepare(sqDomainInsert)
	defer stmt.Close()
	res, err := stmt.Exec(
		id,
		domain,
		key,
	)
	if err != nil {
		return false, err
	}
	_, err = res.LastInsertId()
	if err != nil {
		return false, err
	}
	return true, err
}
