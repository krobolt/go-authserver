FROM golang:latest 
RUN mkdir /go/src/app
ADD . /go/src/app
WORKDIR /go/src/app
RUN go get . && go build -o out
EXPOSE 443
CMD ["/go/src/app/out"]