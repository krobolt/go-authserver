package prometheus

import (
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	LoginRequestsResponseTime = prometheus.NewSummary(prometheus.SummaryOpts{
		Namespace: "AUTHSERVER",
		Name:      "TEST_INPUT",
		Help:      "Test Foobar",
	})

	CriticalError = prometheus.NewSummary(prometheus.SummaryOpts{
		Namespace: "Authserver",
		Name:      "Critical_Error",
		Help:      "Authserver Critial Error",
	})

	Anotherop = promauto.NewCounter(prometheus.CounterOpts{
		Name: "myapp_processed_ops_total",
		Help: "The total number of processed events",
	})

	Temps = prometheus.NewHistogram(prometheus.HistogramOpts{
		Name:    "pond_temperature_celsius",
		Help:    "The temperature of the frog pond.", // Sorry, we can't measure how badly it smells.
		Buckets: prometheus.LinearBuckets(0, 5, 5),   // start :0, 5 buckets, each 5 centigrade wide. 0, 5, 10, 15, 20, 25
	})

	OpsQueued = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace: "our_company",
			Subsystem: "blob_storage",
			Name:      "ops_queued",
			Help:      "Number of blob storage operations waiting to be processed, partitioned by user and type.",
		},
		[]string{
			// Which user has requested the operation?
			"user",
			// Of what type is the operation?
			"type",
		},
	)
)

func init() {
	prometheus.MustRegister(LoginRequestsResponseTime)
	prometheus.MustRegister(CriticalError)
	prometheus.MustRegister(Temps)
	prometheus.MustRegister(OpsQueued)
}

func recordGuageVec() {
	go func() {
		for {
			// Increase a value using compact (but order-sensitive!) WithLabelValues().
			OpsQueued.WithLabelValues("bob", "put").Add(4)
			// Increase a value with a map using WithLabels. More verbose, but order
			// doesn't matter anymore.
			OpsQueued.With(prometheus.Labels{"type": "delete", "user": "alice"}).Inc()
		}
	}()
}

func recordTemp() {
	go func() {
		for {
			Temps.Observe(100)
		}
	}()
}

func recordMetrics() {
	go func() {
		for {
			//opsProcessed.Inc()
			time.Sleep(2 * time.Second)
		}
	}()
}
